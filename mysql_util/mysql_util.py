# _*_ coding:utf-8 _*_

import pymysql


'''
获取数据库链接
'''
def getDBConnect(host,port,username,password,db_name):
    db_connect = pymysql.connect(host=host,user=username,password=password,port=int(port),db=db_name)
    return db_connect






'''
获取指定数据库的所有表的字典
'''
def  getAllTablesFromTheSchema(db_connect,db_name):
    cursor = db_connect.cursor()
    cursor.execute("select table_name,table_comment from information_schema.tables where table_schema='%s' and table_type='base table'" % db_name)
    tables = cursor.fetchall()
    tableDict = {}
    for table in tables:
       tableDict[table[0]]=table[1]
    return tableDict


def getTheTableDetail(db_connect,db_name,table_name):
    cursor = db_connect.cursor()
    cursor.execute("select COLUMN_NAME,COLUMN_TYPE,COLUMN_DEFAULT,COLUMN_COMMENT,COLUMN_KEY from information_schema.COLUMNS where table_schema='%s' and table_name='%s'"% (db_name,table_name))
    table_detail = cursor.fetchall()
    return table_detail



if __name__=='__main__':
    db_connect=getDBConnect('localhost',3306,'root','root','cmrims')
    getTheTableDetail(db_connect,'cmrims','alarm_record')
    
    