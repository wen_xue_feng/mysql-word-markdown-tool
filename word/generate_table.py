# _*_ coding:utf-8 _*_

import docx
from docx import Document
from mysql_util import mysql_util



'''
读写指定 word 文档的表格内容

param filePath word 模板文件绝对路径
'''
 
"""
创建数据库表预览
:param db_connect: 数据库链接
:param storage_path: word 文档存储路径
:param db_name: 数据库名称
:return:
"""
def create_schema_overview_table_on_docx(db_connect,storage_path,db_name):
    document = Document()
    p = document.add_paragraph()
    run = p.add_run('数据库表预览:\n')
    # 所有的数据库表字典
    all_table_overview_dict = mysql_util.getAllTablesFromTheSchema(db_connect,db_name)
    schema_tables_docx = document.add_table(rows=len(all_table_overview_dict)+1, cols=2,style='Table Grid')
    schema_tables_docx.cell(0,0).text = '表名'
    schema_tables_docx.cell(0,1).text = '功能说明'
    mark = 1
    for s_key in all_table_overview_dict.keys():
        schema_tables_docx.cell(mark,0).text= s_key
        schema_tables_docx.cell(mark,1).text= all_table_overview_dict[s_key]
        mark+=1
    p2 =  document.add_paragraph()
    run2 = p2.add_run('\n\n数据库表结构描述: \n\n\n')
    return document


'''
   给指定表建立字段描述表格
   :param db_connect: 数据库链接
   :param table_name 数据库名称
   :param table_chinese 数据库中文名称
   :param table_description 字段描述 tuple 
   :param document document 类型
   :return document word Document 类型
'''
def create_schema_tables_on_docx(db_connect,table_name,table_chinese,table_description,document):
    p = document.add_paragraph()
    run = p.add_run('\n\n'+table_name+'('+table_chinese+')'+'\n\n')
    rows = len(table_description)+3
    cols = 5
    schema_table_detail = document.add_table(rows=rows, cols=cols,style='Table Grid')
    
    schema_table_detail.cell(0,0).text='表名'
    schema_table_detail.cell(0,1).merge(schema_table_detail.cell(0,4))
    schema_table_detail.cell(0,1).text=table_name
    schema_table_detail.cell(rows-1,0).text='补充'
    schema_table_detail.cell(rows-1,1).merge(schema_table_detail.cell(rows-1,4))
    schema_table_detail.cell(rows-1,1).text=table_chinese
    schema_table_detail.cell(1,0).text='列名'
    schema_table_detail.cell(1,1).text='说明'
    schema_table_detail.cell(1,2).text='类型'
    schema_table_detail.cell(1,3).text='空/非空'
    schema_table_detail.cell(1,4).text='约束条件'
    mark=2
    for t in table_description:
        print('t 的 value---',t)
        # 列名
        schema_table_detail.cell(mark,0).text=t[0]
        # 说明
        schema_table_detail.cell(mark,1).text=t[3]
        # 类型定义
        schema_table_detail.cell(mark,2).text=t[1]
        # 空/非空
        if t[2]==None and t[4]!='PRI':
            schema_table_detail.cell(mark,3).text='NULL'
        elif t[4]=='PRI':
            schema_table_detail.cell(mark,3).text='NOT NULL'
        # 约束条件
        if t[4]=='PRI':
            schema_table_detail.cell(mark,4).text='主键'
        else:
            schema_table_detail.cell(mark,4).text=t[4]
        mark = mark+1
    
    return document


'''
编写数据库设计文档
:param db_connect 数据库链接
:param storage_path 存储路径
:param db_name 数据库名称
:return 
'''
def create_db_design_docs(db_connect,storage_path,db_name,time):
    try:
        document = create_schema_overview_table_on_docx(db_connect,storage_path,db_name)
    except:
        print("6666")
            # 所有的数据库表字典
    all_table_overview_dict = mysql_util.getAllTablesFromTheSchema(db_connect,db_name)
    for a in all_table_overview_dict.keys():
        try:
            table_description = mysql_util.getTheTableDetail(db_connect,db_name,a)
            document = create_schema_tables_on_docx(db_connect,a,all_table_overview_dict[a],table_description,document)
        except:
            print("FUCK")
        finally:
            print("FUCK")
        document.save(storage_path+'\\'+db_name+'-'+time+'.docx')
    





