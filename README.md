# mysql-word-markdown-tool


##  ！！！！直接下载 exe 文件使用 程序 [下载链接]

#### 介绍
- 是一个 EXE 文件，点击即可执行。目前仅支持在 windows 上运行。因为我是一个微软粉，所以只编译了windows版本的可执行文件。😄 生成 word 格式的数据库设计文档。


#### 软件架构
    |-- version_record.md 版本改进记录
    |-- word  
        |-- generate_table.py   生成 word 文档方法
        |-- table_template.docx  模板文件
        |-- __init__.py
    |-- ui
        |-- schema_docs_design.ui 界面文件
        |-- ui.png  引用的图片
    |-- mysql_util
        |-- __init__.py
        |-- mysql_util.py  数据库类方法
    |-- bin
        |-- apprcc_rc.py  图标资源文件
        |-- mysql_design_tools.py  启动程序入口
        |-- schema_docs_design.py  
    |--qrc
        |-- apprcc.qrc 图片转 base64 编码
        |---setting.ico 图标
    



#### 安装教程
0. 安装 python 3.我用的是 python 3.6


1. 建议大家开启python 的虚拟环境然后再安装依赖 
~~~shell
    pip install requirements.txt
~~~

2. 安装 PyQt5 相关库
~~~shell
   pip install PyQt5 -i https://pypi.douban.com/simple
   pip install PyQt5-tools -i https://pypi.douban.com/simple
~~~

3. 安装 PyInstaller
~~~shell
   pip install PyInstaller -i https://pypi.douban.com/simple
~~~
4. 编译命令生成 EXE 文件 mmwt 是我用的虚拟环境的目录，建议大家修改成自己的虚拟环境
~~~shell
    pyinstaller --path .\mmwt\Lib\site-packages\PyQt5\Qt\bin   -F -w .\bin\mysql_design_tools.py
~~~

5. ui 文件转换为 py 文件代码


#### 使用说明

0. exe 文件[下载链接]，点击后即可进行下载

[下载链接]: https://gitee.com/coder_xiaozhao/mysql-word-markdown-tool/attach_files/326285/download
1. 点击 EXE 文件,出现如下图的界面
   
   ![程序界面](./ui/ui.png)


2. 输入 MySQL 所在服务器IP，端口，用户名，密码，数据库名称后点击生成一个 docx 文件即完成了




