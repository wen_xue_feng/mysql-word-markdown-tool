# -*- coding: utf-8 -*-
import sys, os
if hasattr(sys, 'frozen'):
    os.environ['PATH'] = sys._MEIPASS + ";" + os.environ['PATH']

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSlot,QDir,QDate,QTime,QDateTime
from PyQt5.QtWidgets import (QMainWindow,QApplication,QDesktopWidget,QToolTip,QLineEdit,QMessageBox,QFileDialog,QDateTimeEdit,
)
from PyQt5.QtGui import QFont,QIntValidator
import  PyQt5.sip
import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from bin.schema_docs_design import Ui_MainWindow
from word import generate_table
from mysql_util import mysql_util
#from bin import apprcc_rc


class Application(QMainWindow,Ui_MainWindow):
    def __init__(self, parent=None):
        super(Application,self).__init__(parent)
        self.center()
        self.setWindowIcon(QtGui.QIcon(":/setting.ico"))
        QToolTip.setFont(QFont('SansSerif',12))
        self.setToolTip('数据库文档生成器')
        self.setupUi(self)
        self.mysql_ip.setPlaceholderText("数据库服务器 IP 地址")
        self.mysql_port.setMinimum(1)
        self.mysql_port.setMaximum(40000)
        self.mysql_port.setSingleStep(1)
        self.mysql_port.setValue(3306)
        self.username.setPlaceholderText("用户名")
        self.password.setPlaceholderText("密码")
        self.password.setEchoMode(QLineEdit.Password)
        self.db_name.setPlaceholderText("数据库名称")
        self.generate_button.clicked.connect(self.msg)
        self.file_location.clicked.connect(self.get_files_location)
        
        self.generate_time.setDisplayFormat("yyyy-MM-dd HH:mm:ss")
        self.generate_time.setMinimumDate(QDate.currentDate().addDays(-1))
        self.generate_time.setMaximumDate(QDate.currentDate().addDays(1))
        self.generate_time.setCalendarPopup(True)


    
    def get_files_location(self):
        file_location=""
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.Directory)
        # 返回选中的文件夹路径
        file_location = dlg.getExistingDirectory()
        print(type(file_location))
        self.label_7.setText(file_location)
        



        



    def msg(self):
        # 使用提问对话框
        ask = QMessageBox.question(self,"数据库设计文档生成器","请再确认一遍填写内容是否准确，程序将生成数据库设计文档",QMessageBox.Yes|QMessageBox.No
        ,QMessageBox.Yes)
        if ask==QMessageBox.Yes:
            self.on_pushButton_clicked()

    
    def warning(self,result):
        if result==False:
           warning = QMessageBox.about(self,"错误","数据库链接失败,重新检查填写内容！")
        else:
           warning = QMessageBox.about(self,"生成成功","数据库设计文档已生成在指定目录下面")
        


    '''
    窗口居中显示
    
    :return 
    '''
    def center(self):
        # 获取屏幕大小
        screen =QDesktopWidget().screenGeometry()
        # 获取应用程序界面大小
        size = self.geometry()
        self.move((screen.width()-size.width())/2,(screen.height()-size.height())/2)


    @pyqtSlot()
    def on_pushButton_clicked(self):
        ip=self.mysql_ip.text()
        port = self.mysql_port.value()
        username = self.username.text()
        
        password = self.password.text()
        db_name = self.db_name.text()
        time = self.generate_time.dateTime().toString("yyyyMMddHHmmss")
        try:
            db_connect = mysql_util.getDBConnect(ip,int(port),username,password,db_name)
        except:
            self.warning(False)
            return False
        storage_path = self.label_7.text()+'\\'
        print(storage_path)
        
        result = generate_table.create_db_design_docs(db_connect,storage_path,db_name,time)
        self.warning(True)
        


if __name__=='__main__':
    import sys
    app = QApplication(sys.argv)


    ui=Application()
    ui.show()
    sys.exit(app.exec_())

